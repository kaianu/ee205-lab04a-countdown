///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
//   @todo
//
// @author TODO_name <TODO_eMail>
// @date   TODO_dd_mmm_yyyy
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int main() {

   time_t now;
   struct tm * timeinfo;
   int year, month, day;
   double seconds, minutes, hours, days, months, years;

   year = 2021;
   month = 5;
   day = 5;
   
   time ( &now);
   timeinfo = localtime (&now);
   timeinfo->tm_year = year;
   timeinfo->tm_mon = month;
   timeinfo->tm_mday = day; 

   mktime (timeinfo);
   
   seconds = difftime(now, mktime(timeinfo));
   
   
   printf ("%.f seconds till date in the current timezone.\n", seconds);

   return 0;
}
